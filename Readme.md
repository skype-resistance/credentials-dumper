# Skype 7 credentials dumper

## Description

Dump Skype 7 login MD5 hash from stored credentials in config.xml. Optionally validate user supplied login/password against stored hash.

The hash is in MD5 format that uses the username, a static string `\nskyper\n` and the  password. The resulting MD5 is stored in the `config.xml` file for the user  after being XOR'd against a key generated by applying 2 SHA1 hashes of "salt" data which is stored in ProtectedStorage using the Windows API CryptProtectData against the MD5.

## Usage

See [Get-SkypeCredentials.md](Get-SkypeCredentials.md) for full help.

### Examples

#### Get login hash from `config.xml` in current directory

```none
Get-SkypeCredentials.cmd
```

#### Get login hash from `config.xml` in Skype 7 profile

```none
Get-SkypeCredentials.cmd -Path "%APPDATA%\Skype\mylogin\config.xml"
```

#### Get login hash from `config.xml` in Skype 7 profile, validate user credentails

```none
Get-SkypeCredentials.cmd -Path "%APPDATA%\Skype\mylogin\config.xml" -Login "mylogin" -Password "mypassword"
```

## Output

```none
Get-SkypeCredentials.cmd -Login aaa -Password bbb

Salt            : a1 2f 40 e1 c6 fb e4 a1 6c 2a 5e 27 78 4d 3a 53 3b 86 57 a9 96 4e 21 2f
AES key         : c6 8b 3a 68 0a 80 28 21 57 78 b3 3f 32 b5 e8 6d 5e ce a3 46 c8 95 83 db db 4d f0 85 bb 83 54 60
XOR key         : b0 0e 2f 39 44 22 4e 00 5f fd de 5f 55 8f b7 15
Login MD5       : 21 e6 fc 21 06 0f 6c 34 13 a1 85 42 ad d5 eb dc
Your  MD5       : 1a c7 bc f8 c9 31 f5 13 c8 6c e6 39 0a 9a 57 45

> Hashes don't match!
```